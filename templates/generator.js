'use strict';

const chalk = require('chalk');

class Generator {
    initializing(configuration) {
        console.log(chalk.blue(`Initializing`));
        this.configuration = configuration;
        console.log(this.configuration.yeoman.sourceRoot());
        console.log(this.configuration.yeoman.destinationPath());
        console.log(this.configuration.yeoman.templatePath());
    }

    prompting() {
        console.log(chalk.blue(`Prompting`));
    }

    configuring() {
        console.log(chalk.blue(`Configuring`));
    }

    default() {
        this._method1();
        this._method2();
    }

    _method1() {
        console.log(chalk.blue(`Default 1`));
    }

    _method2() {
        console.log(chalk.blue(`Default 2`));
    }

    writing() {
        console.log(chalk.blue(`Writing`));
        this.configuration.yeoman.fs.copy(
            this.configuration.yeoman.templatePath('hello.world.html'),
            this.configuration.yeoman.destinationPath('public/hello.world.html'),
        );
    }

    conflicts() {
        console.log(chalk.blue(`Conflicts`));
    }

    install() {
        console.log(chalk.blue(`Install`));
    }

    end() {
        console.log(chalk.blue(`End`));
    }
};

module.exports = new Generator();