## Yeoman example template

This repository features an example _Yeoman_ template that will 
be fetched and executed by the _Yeoman external templates POC_.

The motivation for this _POC_ is to create an easily maintainable 
code generator that doesn't force the user to update the generator 
in order to use the latest templates.

An example of really good features would be:
- [ ] Automatic '_new versions_' check and fetch after user validation.  
ie: new generator version is available and user is asked whether he 
wants to fetch the update or not.
- [ ] Version management within the main generator.  
ie: when executing a generation, user is asked which version of the 
generator he'd like to use in case there's more than one.